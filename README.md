
### VMware AppCatalyst Lab Installer v2.6
![alt text](http://static1.squarespace.com/static/50203aca84ae954efd2f9b70/t/55dd50b3e4b087612e6d759e/1455081593314/?format=250w "cloudcanuck")
###### Author: Paul Gifford <pgifford@vmware.com>
###### Blog Post: http://www.cloudcanuck.ca/posts/appacatalyst-lab-installer
###### Lessons Learned from and special thanks to: Sebastian Weigand <sweigand@vmware.com>

###### Automated setup for installing a VMware AppCatalyst Lab on your Mac

###### Prerequisites:
  - OSX Yosemite and  El Capitan (not tested on the Beta or Developer Previews)
  - Terminal
  - Internet Access

###### Changelog: 
  - 2.6 - Added Docker Toolbox install
  - 2.5 - Added Updated to include Vagrant 1.8.0
  - 2.4 - Added AppCatalyst Daemon start-up options
  - 2.3 - Added Python install via homebrew
  - 2.2 - Added Docker Compose from Docker Toolbox
  - 2.1 - Added Packer install via homebrew
  - 2.0 - Complete re-write with logging and error checking 

###### Install:
```
curl -O https://raw.githubusercontent.com/pauldgifford/appcatalystlab/master/appcatalyst-lab.sh
```

**Disclaimer: Use at your own risk, this has been tested on default installs of OS X Yosemite and El Capitan without any customizations.  This has not been tested on the Beta or Developer Previews from Apple.**
