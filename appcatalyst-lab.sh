#!/usr/bin/env bash

# =============================================================================
# VMware AppCatalyst Lab Installer v2.6
# =============================================================================
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# AppCatalyst Lab: Automated install of VMware AppCatalyst Lab
# Author: Paul Gifford <pgifford@vmware.com>
# Blog Post: http://www.canuck.io/posts/appacatalyst-lab-installer
# Lessons Learned from: Sebastian Weigand <sweigand@vmware.com>
#

LOGFILE=appc_lab_install.log
APPCATALYST_URL=http://getappcatalyst.com/downloads/VMware-AppCatalyst-Technical-Preview-August-2015.dmg
APPCDAEMON_URL=https://raw.githubusercontent.com/paulgifford/appcatalystlab/master/com.vmware.appcatalyst.daemon.plist
VAGRANT_URL=https://releases.hashicorp.com/vagrant/1.8.0/vagrant_1.8.0.dmg
DOCKERTOOLBOX_URL=https://github.com/docker/toolbox/releases/download/v1.9.1f/DockerToolbox-1.9.1f.pkg
DOCKERCLIENT_URL=https://get.docker.com/builds/Darwin/x86_64/docker-1.7.1
DOCKERMACHINE_URL=https://github.com/cloudnativeapps/machine/releases/download/vmw_appcatalyst_v0.1/docker-machine_darwin-amd64

# Colors
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"
COL_MAGENTA=$ESC_SEQ"35;01m"

clear
echo ""
echo -e "$COL_GREEN AppCatalyst Lab Installer v2.6 $COL_RESET"
echo ""
read -p "Press [Enter] to begin..."


# Write everything to the screen, and also to a log file.
echo -e "\n\nBegan new run of AppCatalyst Lab Installer at $(date +%T) on $(date +%m/%d/%y)" >> $LOGFILE
exec > >(tee -a $LOGFILE)
echo ""

# =================================================================================
# Install VMware AppCatalyst Lab components
# =================================================================================

# I consider Homebrew, git, wget, and homebrew-based Python (which gives pip),
# Vagrant (with plugins), Docker Toolbox, Docker Machine and Ansible
# to be essential tools for the AppCatalyst Lab.
#
# Strictly speaking, we don't need them as they're wrapped in the dev-photon-vm,
# but for future use cases in the AppCatalyst Lab, we will install them beforehand.

COUNT=0

# Recurse this to achieve a somewhat idempotent execution:
check_system() {

  STATE=OK

  # In case we loop too many times, keep this equal to # of ifs:
  let COUNT+=1
  if [ $COUNT -gt 6 ]; then
    echo -e "$COL_RED Something happened and this script got stuck in a loop, please have a look and correct. Please check $LOGFILE. $COL_RESET"
    exit 1
  fi

  if [ $COUNT -eq 1 ]; then
    CHECK_VERB="Checking"
  else
    CHECK_VERB="\nRechecking"
  fi

  echo -e "$COL_GREEN ${CHECK_VERB} your Mac for AppCatalyst Lab Setup... $COL_RESET"
  echo ""

  # Check for Homebrew
  echo -n "Homebrew:                             "
  if command -v brew &> /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  # No Homebrew:
  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Requesting an install of Homebrew..."
    echo ""
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    echo ""
    STATE=Bad
  fi

  # Check for wget, because curl isn't as sexy as wget:
  echo -n "wget:                                 "
  if command -v wget > /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Installing wget..."
    brew install wget >> $LOGFILE 2>&1
    echo ""
    STATE=Bad
  fi

  # Check for git:
  echo -n "git:                                  "
  if git --version | grep -qv Apple > /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Installing git..."
    brew install git >> $LOGFILE 2>&1
    echo ""
    STATE=Bad
  fi

  # Check for Ansible in Homebrew:
  echo -n "Ansible:                              "
  if brew list ansible &> /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Installing Ansible..."
    brew install ansible >> $LOGFILE 2>&1
    echo ""
    STATE=Bad
  fi

    # Check for Python in Homebrew:
  echo -n "Python:                               "
  if brew list python &> /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Installing Python..."
    brew install python >> $LOGFILE 2>&1
    echo ""
    STATE=Bad
  fi

  # Check for Packer in Homebrew:
  echo -n "Packer:                               "
  if brew list packer &> /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Installing Packer..."
    brew install packer >> $LOGFILE 2>&1
    echo ""
    STATE=Bad
  fi

  # Check and Install AppCatalyst:
  echo -n "AppCatalyst:                          "
  if pkgutil --pkg-info com.vmware.pkg.AppCatalyst &> /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Downloading AppCatalyst..."
    wget -c -q $APPCATALYST_URL >> $LOGFILE 2>&1
    echo ""
    echo "Installing AppCatalyst..."
    hdiutil attach VMware-AppCatalyst-Technical-Preview-August-2015.dmg >> $LOGFILE 2>&1
    sudo installer -pkg "/Volumes/VMware AppCatalyst/Install VMware AppCatalyst.pkg" -target / >> $LOGFILE 2>&1
    umount "/Volumes/VMware AppCatalyst/" >> $LOGFILE 2>&1
    export PATH="/opt/vmware/appcatalyst/bin:$PATH" >> $LOGFILE 2>&1
    echo ""
    STATE=Bad
  fi

  # Check and Install Docker Toolbox:
  echo -n "Docker Toolbox:                       "
  if pkgutil --pkg-info io.docker.pkg.docker &> /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Downloading Docker Toolbox..."
    wget -c -q $DOCKERTOOLBOX_URL >> $LOGFILE 2>&1
    echo ""
    echo "Installing Docker Toolbox..."
    sudo installer -pkg "DockerToolbox-1.9.1f.pkg" -target / >> $LOGFILE 2>&1
    echo "Backing up Docker Machine..."
    sudo mv /usr/local/bin/docker-machine /usr/local/bin/docker-machine_toolbox >> $LOGFILE 2>&1
    echo ""
    STATE=Bad
  fi

  # Check and Install Docker Machine (w/ AppCatalyst):
  echo -n "Docker Machine (AppCatalyst Driver):  "
  if [ -f /usr/local/bin/docker-machine ]; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Downloading Docker Machine (w/ AppCatalyst)..."
    echo ""
    sudo mkdir -p /usr/local/bin >> $LOGFILE 2>&1
    sudo curl $DOCKERMACHINE_URL -L -o /usr/local/bin/docker-machine >> $LOGFILE 2>&1
    sudo chmod +x /usr/local/bin/docker-machine >> $LOGFILE 2>&1
    STATE=Bad
  fi

  # Check and Install Vagrant:
  echo -n "Vagrant:                              "
  if pkgutil --pkg-info com.vagrant.vagrant &> /dev/null; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Downloading Vagrant..."
    wget -c -q $VAGRANT_URL >> $LOGFILE 2>&1
    echo ""
    echo "Installing Vagrant..."
    echo ""
    hdiutil attach vagrant_1.8.0.dmg >> $LOGFILE 2>&1
    sudo installer -pkg "/Volumes/Vagrant/Vagrant.pkg" -target / >> $LOGFILE 2>&1
    umount "/Volumes/Vagrant/" >> $LOGFILE 2>&1
    STATE=Bad
  fi

  # Check and Install Vagrant AppCatalyst Plugin:
  echo -n "Vagrant AppCatalyst Plugin:           "
  if [ $(vagrant plugin list | egrep 'vagrant-vmware-appcatalyst' -c) == 1 ]; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Installing Vagrant AppCatalyst Plugin..."
    echo ""
    vagrant plugin install vagrant-vmware-appcatalyst >> $LOGFILE 2>&1
    STATE=Bad
  fi

  # Check and Install Vagrant Photon Plugin:
  echo -n "Vagrant Photon Plugin:                "
  if [ $(vagrant plugin list | egrep 'vagrant-guests-photon' -c) == 1 ]; then
    echo -e "$COL_GREEN[ OK ]$COL_RESET"

  else
    echo -e "$COL_RED[ Missing ]$COL_RESET"
    echo "Installing Vagrant Photon Plugin..."
    echo ""
    vagrant plugin install vagrant-guests-photon >> $LOGFILE 2>&1
    STATE=Bad
  fi

# still need ovftool install
# https://my.vmware.com/web/vmware/details?downloadGroup=OVFTOOL400&productId=353
# requires My VMware Login

}

until [[ $STATE == "OK" ]]; do
  check_system
done

while true; do
echo ""
echo ""
    read -p "Would you like the AppCatalyst Daemon to run on start-up? " yn
    case $yn in
        [Yy]* ) echo "";echo ""; echo "Adding AppCatalyst Daemon start-up config...";sudo curl $APPCDAEMON_URL -o ~/Library/LaunchAgents/com.vmware.appcatalyst.daemon.plist >> $LOGFILE 2>&1; break;;
        [Nn]* ) echo "";echo ""; echo "Removing AppCatalyst Daemon start-up config..."; sudo rm ~/Library/LaunchAgents/com.vmware.appcatalyst.daemon.plist >> $LOGFILE 2>&1; break;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo "Completed last new run of AppCatalyst Lab Installer at $(date +%T) on $(date +%m/%d/%y)" >> $LOGFILE 2>&1
echo ""
echo ""
echo -e "$COL_GREEN Your AppCatalyst Lab is ready! $COL_RESET"
echo ""
echo -e "$COL_GREEN Note: If you chose to have the AppCatalyst Daemon run on start-up, it will only take effect after a reboot. $COL_RESET"
echo ""
echo ""
read -p "Press [Enter] Launch VMware AppCatalyst..."

# Start AppCatalyst
export PATH="/opt/vmware/appcatalyst/bin:$PATH" >> $LOGFILE 2>&1
appcatalyst

# EOF
